require 'db/migrations/utils'

Sequel.migration do

  up do
    alter_table(:carousel_image) do 
      rename_column(:name, :title)
    end
  end

  down do
    alter_table(:carousel_image) do
      rename_column(:title, :name)
    end
  end

end


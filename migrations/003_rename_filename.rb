require 'db/migrations/utils'

Sequel.migration do

  up do
    alter_table(:carousel_image) do 
      rename_column(:filename, :name)
    end
  end

  down do
    alter_table(:carousel_image) do
      rename_column(:name, :filename)
    end
  end

end

require 'db/migrations/utils'

Sequel.migration do

  up do
    add_column :carousel_image, :url, String
  end

  down do
    drop_column :carousel_image_image, :url
  end

end

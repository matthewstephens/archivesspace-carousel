require 'db/migrations/utils'

Sequel.migration do

  up do

    create_table(:carousel_image) do
      primary_key :id

      Integer :lock_version, :default => 0, :null => false
      Integer :json_schema_version, :null => false

      Integer :repository_id, :null => true

      String :filename
      String :description

      apply_mtime_columns
    end

    alter_table(:carousel_image) do
      add_foreign_key([:repository_id], :repository, :key => :id)
    end

  end

  down do
  end

end

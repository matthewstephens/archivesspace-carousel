require 'db/migrations/utils'

Sequel.migration do

  up do
    alter_table(:carousel_image) do 
      add_column :position, :integer 
    end
  end

  down do
    alter_table(:carousel_image) do
      drop_column :position 
    end
  end

end


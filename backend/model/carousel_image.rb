class CarouselImage < Sequel::Model(:carousel_image)
    include ASModel
    corresponds_to JSONModel(:carousel_image)

    set_model_scope :repository

end

#CarouselImage.unrestrict_primary_key

class ArchivesSpaceService < Sinatra::Base

  # Public API endponts

  # test this plugin is available
  Endpoint.get('/carousel_images/ping')
    .description("Test the presense of the carousel image API")
    .params("repo_id", :repo_id)
    .permissions([])
    .returns([200, "hi there"]) \
  do
    json_response({response: 'hi there'})
  end

  # get a specific group of carousel image objects by position
  Endpoint.get('/plugins/repositories/:repo_id/carousel_images/position')
    .description("Get a specific group of Carousel Image records, sorted by their position field")
    .params(["repo_id", :repo_id])
    .permissions([])
    .returns([200, "(:carousel_image)"],
             [404, "Not found"]) \
  do
    where = "position IN (1, 2, 3, 4, 5)"
    handle_unlimited_listing(CarouselImage, where)
  end

  # Admin API endpoints
  @admin_carousel_permissions = [:view_repository, :manage_repository]

  # get a carousel image by its id, restfully
  Endpoint.get('/plugins/repositories/:repo_id/carousel_images/:id')
    .description("Get a list of carousel images by Repository ID")
    .params(["id", :id],
            ["repo_id", :repo_id],
            ["resolve", :resolve])
    .permissions(@admin_carousel_permissions)
    .returns([200, "(:carousel_image)"],
             [404, "Not found"]) \
  do
    json_response(resolve_references(CarouselImage.to_jsonmodel(CarouselImage.get_or_die(params[:id])),params[:resolve]))
  end

  # get a list of carousel images belonging to a repository
  Endpoint.get('/carousel_images')
    .description("Get a list of Carousel Images")
    .params(["repo_id", :repo_id], ["resolve", :resolve])
    .permissions(@admin_carousel_permissions)
    .returns([200, "[(:repository)]"]) \
  do
    handle_unlimited_listing(CarouselImage)
  end

  # get a list of carousel images belonging to a repository, restfully
  Endpoint.get('/plugins/repositories/:repo_id/carousel_images')
    .description("Get a list of Carousel Images")
    .params(["repo_id", :repo_id], ["resolve", :resolve])
    .permissions(@admin_carousel_permissions)
    .returns([200, "[(:repository)]"]) \
  do
    Log.debug( "Endpoint.get recieved: repo_id: #{params[:repo_id]}" )
    handle_unlimited_listing(CarouselImage)
  end

  # create a new carousel image
  Endpoint.post('/plugins/repositories/:repo_id/carousel_images')
    .description("Create a Carousel Image for a repository")
    .params(["repo_id", :repo_id],["carousel_image", JSONModel(:carousel_image), "The record to create", :body => true])
    .permissions(@admin_carousel_permissions)
    .returns([200, :created],
             [400, :error],
             [403, :access_denied]) \
  do
    handle_create(CarouselImage, params[:carousel_image])
  end

  # update a carousel image 
  Endpoint.post('/plugins/repositories/:repo_id/carousel_images/:id')
    .description("Update a Carousel Image for a repository")
    .params(["id", :id],
            ["repo_id", :repo_id],
            ["carousel_image", JSONModel(:carousel_image), "The updated record", :body => true])
    .permissions(@admin_carousel_permissions)
    .returns([200, :updated],
             [404, "Not found"]) \
  do
    Log.info("Updating CarouselImage")
    Log.info(JSONModel(:carousel_image).to_json)
    handle_update(CarouselImage, params[:id], params[:carousel_image])
  end

  # delete a carousel image
  Endpoint.delete('/plugins/repositories/:repo_id/carousel_images/:id')
    .description("Delete a Carousel Image")
    .params(["repo_id", :repo_id],["id", :id])
    .permissions(@admin_carousel_permissions)
    .returns([200, :deleted],
             [400, :error]) \
  do
    Log.info( "Endpoint.delete recieved: id: #{params[:id]} repo_id: #{params[:repo_id]}" )
    handle_delete(CarouselImage, params[:id])
  end
end

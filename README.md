*** Archivesspace Carousel ***

This is a dynamic carousel plugin, which allows users to curate a set of images to be displayed on a page in the public interface of Archivesspace.

If you want a simpler, hard-coded set of images, see the `_carousel.html.erb` partial in `public/views` for an example.

** Installation **

Clone this repository into the `plugins` directory of your AS application.
Run the `initialize-plugin` script in $AS_HOME/scripts.

Run the `setup-database` script as well, to add a database table to store your carousel image data.

** Use **

You must have at least one repository for the plugin's admin interface to work correctly.

Authorized users should see a 'Carousel Images' dropdown under the `Plug-Ins` tab on the right nav bar.
Clicking on this will take you to a simple admin interface to allow you to add/edit/delete whatever images you want to include in your carousel.
These images will be stored inside ArchivesSpace, but can be any valid URL, and will not be indexed or treated as part of your archival collections.

Once a set of images has been saved, this data will be pulled by the Public interface from the backend, and used to render the carousel.
Visit your home page (e.g. http://localhost:8081' ) to see the carousel in action.

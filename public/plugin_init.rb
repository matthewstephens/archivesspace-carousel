Plugins::extend_aspace_routes(File.join(File.dirname(__FILE__), "routes.rb"))
#ArchivesSpacePublic::Application.extend_aspace_routes(File.join(File.dirname(__FILE__), "routes.rb"))
ArchivesSpacePublic::Application.config.after_initialize do
  require_relative 'helpers/carousel_helper'
  require_relative 'controllers/welcome_controller'
end

class WelcomeController < ApplicationController
  include CarouselHelper
  helper_method CarouselHelper.instance_methods

  skip_before_action  :verify_authenticity_token

  @@images = CarouselHelper.get_carousel_images

  def show
    @page_title = I18n.t 'brand.welcome_page_title'
    @search = Search.new(params)

    # call class variable if greater performance is desired
    #@images = @@images ||= CarouselHelper.get_carousel_images 
    @images = CarouselHelper.get_carousel_images 

    render :show, locals: { images: @images }
  end
end

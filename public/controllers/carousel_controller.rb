class CarouselController < ApplicationController
  skip_before_action  :verify_authenticity_token

  # fetch list of image URLs from backend, filter out the
  # data needed and render the view
  def build
    keys_to_retain = %w{ id position url title }
    # initialize array with stock images, which will
    # display if call to the database is unsuccessful
    @images = [
      '/assets/images/image1.jpg',
      '/assets/images/image2.jpg',
      '/assets/images/image3.jpg',
      '/assets/images/image4.jpg',
      '//www.fillmurray.com/200/300'
    ]
    uri = URI.parse AppConfig[:backend_url]
    http = Net::HTTP.new uri.host, uri.port
    request = Net::HTTP::Get.new('/plugins/repositories/2/carousel_images/position')
    response = http.request request
    if response.code == "200"
      @content = response.body
      @parsed = ASUtils.json_parse(@content)
      @parsed.sort! {|a,b| a["position"] <=> b["position"]  }
      @filtered = @parsed.map do |obj| obj.keep_if {|k,v| keys_to_retain.include? k } end
      @images = @filtered.map do |obj| obj["url"] end
    else 
      @content = 'no data'
    end


    render 'carousel/advanced_carousel'

  end
end


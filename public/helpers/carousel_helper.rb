module CarouselHelper

  def hello
    "this is a CarouselHelper hello"
  end

  # fetch list of image URLs from backend, filter out the
  # data needed and render the view
  def self.get_carousel_images 
    keys_to_retain = %w{ id position url title }

    # these hard-coded URLs will serve as a fallback 
    @images = [
      '/assets/images/look.jpg',
      '/assets/images/document.jpg',
      '/assets/images/morehouse_alumnus.jpg',
      '/assets/images/we_demand_equal_respect.jpg',
      '//www.fillmurray.com/200/300'
    ]

    # query the backend for image data, sort and prune the response 
    uri  = URI.parse AppConfig[:backend_url]
    http = Net::HTTP.new uri.host, uri.port
    request  = Net::HTTP::Get.new('/plugins/repositories/2/carousel_images/position')
    response = http.request request

    if response.code == "200"
      @content = response.body
      @parsed  = ASUtils.json_parse(@content)
      @parsed.sort! {|a,b| a["position"] <=> b["position"]  }
      @filtered = @parsed.map do |obj| obj.keep_if {|k,v| keys_to_retain.include? k } end
      @captions = @filtered.map do |obj| obj["title"] end
      @images   = @filtered.map do |obj| obj["url"] end
    else 
      Rails.logger.error(':get_carousel_images got a bad response from the backend server')
      @content = 'no data'
    end

    return @images
  end
end

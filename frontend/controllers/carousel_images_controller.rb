class CarouselImagesController < ApplicationController

  set_access_control "manage_repository" => [:search, :index, :show, :import, :new, :edit, :create, :update, :delete]

  def index
    @carousel_images = CarouselImage.all({repo_id: session[:repo_id]})
    logger.debug( "INDEX ACTION HERE" )
#    logger.info( Rails.application.routes.routes.map(&:name).compact.map { |route| "#{route}_path" } )
    render "carousel_images/index"
  end


  def search
    results = do_search(params)

    render :json => results.to_json
  end

  def show
    logger.debug "CAROUSEL IMAGE CONTROLLER: show #{params[:id]}"
    @carousel_image = CarouselImage.find(params[:id])
    render "carousel_images/show"
  end

  def new
    @carousel_image = CarouselImage.new({repo_id: session[:repo_id]})._always_valid!
    @carousel_image[:repository_id] = session[:repo_id]
    render "carousel_images/new"
  end

  def edit
    logger.debug( "EDIT ACTION HERE" )
    @carousel_image = CarouselImage.find(params[:id])
    @repo_id = session[:repo_id]
    logger.debug "params[:id] = #{params[:id]}"
    logger.debug "@carousel_image = #{@carousel_image}"
  end
  
  def create
    handle_crud(:instance => :carousel_image,
                :model => JSONModel(:carousel_image),
                :on_invalid => ->(){ render action: "new" },
                :on_valid => ->(id){
                    flash[:success] = I18n.t("Success: new image created.", @carousel_image.inspect)
                    redirect_to(:controller => :carousel_images,
                                :action => :show,
                                :id => id, :repo_id => session[:repo_id] ) })
  end

  def update
    logger.debug( "UPDATE ACTION HERE" )
    @carousel_image_params= params[:carousel_image]
    @carousel_image = CarouselImage.find(params[:id])
    @carousel_image.title= @carousel_image_params[:title]
    @carousel_image.description= @carousel_image_params[:description]
    @carousel_image.url= @carousel_image_params[:url]
    @carousel_image.position= @carousel_image_params[:position].to_i
    @utf8= params[:utf8]
    @authenticity_token= params[:authenticity_token]
    response = @carousel_image.save
    redirect_to(:controller => :carousel_images,
                :action => :show,
                :id => params[:id], :repo_id => session[:repo_id] )
  end

  def delete
    logger.debug( "DELETE ACTION HERE" )
    carousel_image = JSONModel(:carousel_image).find(params[:id])

    begin
      class_url = carousel_image.class.my_url(carousel_image.id)
      full_request_url = URI( "#{class_url}/#{carousel_image[:id]}" )
      logger.debug("sending -XDELETE to #{full_request_url}")
      response = JSONModel::HTTP::delete_request(full_request_url)
      logger.info(response.inspect)
    rescue Exception => e
      logger.error(e.inspect)
      flash[:error] = "There has been an error: #{e.message}"
      return
    end
    flash[:success] = "Successful deletion of Carousel Image #{params[:id]}"
    redirect_to(:controller => :carousel_images, :action => :index, :deleted_uri => carousel_image.uri)
  end

  def destroy
    logger.debug( "DESTROY ACTION HERE" )
    carousel_image = JSONModel(:carousel_image).find(params[:id])
    logger.debug("Destroy() found carousel_image: #{carousel_image}")
    carousel_image.delete
    flash[:success] = "Successful deletion of Carousel Image #{params[:id]}"
    redirect_to(:controller => :carousel_images, :action => :index, :deleted_uri => carousel_image.uri)
  end

  private

end

Rails.application.routes.draw do

  scope '/plugins' do
    resources :carousel_images
    match 'carousel_images/:id/delete' => 'carousel_images#delete', :via => [:post]
    match 'carousel_images/:id' => 'carousel_images#update', :via => [:post]
  end

end

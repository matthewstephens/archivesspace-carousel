ArchivesSpace::Application.extend_aspace_routes(File.join(File.dirname(__FILE__), "routes.rb"))
ArchivesSpace::Application.config.after_initialize do
  require_relative 'helpers/application_helper'
end

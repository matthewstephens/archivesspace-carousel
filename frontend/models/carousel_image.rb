class CarouselImage < JSONModel(:carousel_image)

  def id
    return self[:id].to_s
  end

  def repository_id
    return self[:repository_id].to_s
  end

  # form helper calls this method, so we're adding it
  def display_string(json)
    return json['title']
  end
end

{
  :schema => {
    "$schema" => "http://www.archivesspace.org/archivesspace.json",
    "version" => 1,
    "type" => "object",
    "uri" => "/plugins/repositories/:repo_id/carousel_images",

    "properties" => {
        "id" => {"type" => "integer", "required" => false},
        "repository_id" => {"type" => "integer", "required" => false},
        "position" => {"type" => "integer", "required" => false},
        "publish" => {"type" => "boolean", "required" => false},
        "title" => {"type" => "string", "maxLength" => 255, "required" => false},
        "url" => {"type" => "string", "maxLength" => 2048, "required" => false},
        "description" => {"type" => "string","maxLength" => 255, "required" => false}
    },
  },
}
